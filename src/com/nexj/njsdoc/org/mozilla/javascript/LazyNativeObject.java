package com.nexj.njsdoc.org.mozilla.javascript;

/**
 * 
 */
public class LazyNativeObject extends NativeObject implements Callable {

    private static final long serialVersionUID = -8447909989551118954L;

    public LazyNativeObject() {
    }

    /**
     * @see com.nexj.njsdoc.org.mozilla.javascript.Callable#call(com.nexj.njsdoc.org.mozilla.javascript.Context, com.nexj.njsdoc.org.mozilla.javascript.Scriptable, com.nexj.njsdoc.org.mozilla.javascript.Scriptable, java.lang.Object[])
     */
    public Object call(Context cx, Scriptable scope, Scriptable thisObj, Object[] args) {
        return new LazyNativeObject();
    }
}
