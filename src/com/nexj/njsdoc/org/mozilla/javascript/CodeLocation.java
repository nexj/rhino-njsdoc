package com.nexj.njsdoc.org.mozilla.javascript;

/**
 * 
 */
public class CodeLocation implements Comparable<CodeLocation>
{
   public final String file;

   public final int line;

   public CodeLocation(String file, int line) {
      this.file = file;
      this.line = line;
   }

   /**
    * @see java.lang.Object#toString()
    */
   public String toString() {
      return file + ":" + line;
   }

   /**
    * @see java.lang.Object#hashCode()
    */
   @Override
   public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((file == null) ? 0 : file.hashCode());
      result = prime * result + line;
      return result;
   }

   /**
    * @see java.lang.Object#equals(java.lang.Object)
    */
   @Override
   public boolean equals(Object obj) {
      if (this == obj)
         return true;
      if (obj == null)
         return false;
      if (getClass() != obj.getClass())
         return false;
      CodeLocation other = (CodeLocation)obj;
      if (file == null) {
         if (other.file != null)
            return false;
      }
      else if (!file.equals(other.file))
         return false;
      if (line != other.line)
         return false;
      return true;
   }
    
    /**
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(CodeLocation o) {
        int c = file.compareTo(o.file);
        
        if (c == 0) {
            c = line - o.line;
        }
        
        return c;
    }
}
